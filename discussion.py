# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol


#[Section] Python Syntax
# Hello World in Python
print("Hello World!")

# [Section] Indentation
# Where  in other programming languages the indentation in code is for readbility only, the indentation in Python is very important
# In Python, indentation is used to indicate a block of code.
# Similar to JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the container of data
# In Python, a variable is declare by stating the variable name and assigning a value using the equalit symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
#All identifiers should begin with a letter(A to Z or a to z), dollar sign or an underscore.
# after the first character, identifier can have any combination of characters
# Unlike JavaScript that uses the camel csing, Python uses the snake case convention for variables as defined in the PEP (Python enhancement proposal).
# Most importantly, identifiers are case sensitive.
age = 35
middle_initial = "C"
# Python allows assigning values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# [Section] Data Types
# Data types convey what kind of information a variable hlds. There are different data types and each has its own use.

# In python, there are the commomly used data types:
# 1. Strings(str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers(int, float, complex) - for intergers, decimal and complex numbers
num_of_days = 365 #This is an integer
pi_approx = 3.1416 #This is a float
complex_num = 1 + 5j #This a complex number, letter j represents the imaginary

print(type(full_name))

# 3. Boolean(bool) - for truth values
# Boolean values in Python atart with uppercase letters
isLearning = True
isDifficult = False

#[Section] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

print(full_name + " " + secret_code)

#[Section] Terminal Outputs
# In python, printing in the terminal uses the print() function
# To use variables, concatenate (+ symbol) between strings can be used 
# However, we cannot concatenate string to a number or to a different data type
# print("My age is " + age)

# [Section] Typecasting
# here are some functions that can be use in typecastins
#1. int() - convert the value into an integer
print(int(3.75))
#2. float() - converts the value into an integer value
print(float(5))
#3. str() - converts the value into string
print("My age is " + str(age))

# Another way to avoid type erro in printing without the use of typecasting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}.")

# [Section] Operations
# Python has operator familes that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators - used to assign to variables
num1 = 4

num1 += 3
print(num1)

#other operators -=, *=, /=, %=

# Comparison operators - used compare values (boolean values)

print(1 == "1")
#other operators, !=, >=, <=, > , <

# Logical operators - used to combine conditional statements
# Logical Or operator
print(False and True or True)
# Logical And Operator
print(True and False)
# Logical Not Operator
print(not False)




